import 'package:flutter/material.dart';

import 'AdressDetails.dart';

class AdressList extends StatefulWidget {
  @override
  _Screen2List createState() => _Screen2List();
}

class _Screen2List extends State<AdressList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('List of all Adress'
          ),
        //  backgroundColor: Colors.blueAccent),
      ),
      body: BuildView(context)
    );
  }

  ListView BuildView(BuildContext context){
    return ListView.builder(
        itemCount: 10,
        itemBuilder: (_,index){
      return ListTile(
        title : Text('the list item #$index'),
        subtitle: Text('The adress'),
        leading: const Icon(Icons.location_on),
        trailing: const Icon(Icons.arrow_forward),
        onTap: (){
       Navigator.push(context, MaterialPageRoute(builder: (context) =>AdressDetails(index)));

        },

      );
    },
    );
  }
}
