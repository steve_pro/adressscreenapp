import 'package:flutter/material.dart';

import 'EnterAdressPage.dart';

void main()=> runApp( MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return  MaterialApp(
      debugShowCheckedModeBanner: false,
      home:EnterAdressPage(),
    );
  }
  //const MyApp({Key? key}) : super(key: key);


}
