

import 'package:adress_app/Adress.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';


class DatabaseHelper{

static  DatabaseHelper databaseHelper;// singleton DatabaseHelper
static  Database database;

// define column table
  String AdressTable ='adress';
String colName ='name';
 String colStreet= 'street';
 String colCode='zip_code';
String colCity= 'city';

DatabaseHelper._createInstance(); // named constructor to create instance of databasehelper

factory DatabaseHelper(){
  if(databaseHelper == null){
    databaseHelper= DatabaseHelper._createInstance();// execute only once singleton object
  }
  return databaseHelper;
}
 Future<Database> get Database async {
  if(database== null){
    database= await intializeDB();

  }
  return database;

}


void _createDb(Database db, int newVersion) async{
  await db.execute('CREATE TABLE $AdressTable($colName TEXT , $colStreet TEXT,$colCode INTEGER ,$colCity TEXT)');
}

Future<Database>intializeDB()async{
  // get the directory for storage database
  Directory directory = await getApplicationDocumentsDirectory();
  String path = directory.path+ 'adress.db';
  // open and create database
  var adressDatabase= await openDatabase(path, version: 1, onCreate:  _createDb);
  return adressDatabase;

}
// get all Adress from DB
 Future <List<Map<String,dynamic>>> getAdressList() async{
  Database db = await this.Database;
return await db.query(AdressTable);
}
Future <int> InsertAdress(Adress adress)async{
  Database db = await this.Database;


  return await db.insert(AdressTable, adress.toMap());

}

}