import 'package:adress_app/AdressList.dart';
import 'package:flutter/material.dart';

import 'Button.dart';
import 'InputField.dart';

class InputWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30),
      child: Column(
        children: <Widget>[
          SizedBox(height: 40,),
          Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10)
            ),
            child: InputField(),
          ),
            SizedBox(height: 40,),
            TextButton(
    onPressed: () {
      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>AdressList()));
      },
    child: Text(
    'Select Already saved Adress',
    style: TextStyle(
    decoration: TextDecoration.underline,
    color: Color(0xff4c505b),
    fontSize: 18,
    ),)),
          //DGDG
      //    SizedBox(height: 40,),
    //      Text(
    //        "Select Already saved Adress",
    //        style: TextStyle(color: Colors.grey),
      //    ),
          SizedBox(height: 40,),
          Button()
        ],
      ),
    );
  }
}

